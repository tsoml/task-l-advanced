package eu.tsoml.model;


import eu.tsoml.util.Converter;

public class Exchanger extends FinancialInstitution implements Converter {

    private Currency[] currencies;

    public Exchanger(String name, String address, Currency[] currencies) {
        super(name, address);
        this.currencies = currencies;
    }

    @Override
    public double convert(double amount, String userCurrency, String requiredCurrency) {
        double result = 0;
        if (userCurrency.equalsIgnoreCase("UAH")) {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(requiredCurrency)) {
                    result = amount / currency.getRate();
                }
            }
        } else {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(userCurrency)) {
                    result = amount * currency.getRate();
                }
            }
        }
        return result;
    }

    public String getAvailableCurrencies() {
        StringBuilder availableCurrencies = new StringBuilder();
        for (Currency currency : currencies) {
            availableCurrencies.append(currency.getName()).append(" ");
        }
        return availableCurrencies.toString();
    }

    @Override
    public String toString() {
        return String.format("%s, available currencies for convert: %s", super.toString(), getAvailableCurrencies());
    }
}
