package eu.tsoml.model;

import eu.tsoml.util.Sender;

public class OnlineTranslationService extends FinancialInstitution implements Sender {

    private double senderFee;

    public OnlineTranslationService(String name, String address, double senderFee) {
        super(name, address);
        this.senderFee = senderFee;
    }

    @Override
    public double send(double money) {
        return money * (1 - senderFee);
    }


    @Override
    public String toString() {
        return String.format("%s; sender fee: %s", super.toString(), senderFee);
    }

}
