package eu.tsoml.model;

import eu.tsoml.util.Converter;
import eu.tsoml.util.Deposit;
import eu.tsoml.util.Loan;
import eu.tsoml.util.Sender;

public class Bank extends FinancialInstitution implements Converter, Loan, Deposit, Sender {

    private Currency[] currencies;
    private double converterLimit;
    private double converterFee;
    private double loanLimit;
    private double loanInterestRate;
    private double depositMaxLength;
    private double senderFee;
    private double senderAdditionalFee;
    private int yearOfLicense;

    public Bank(String name, String address, Currency[] currencies, double converterLimit, double converterFee, double loanLimit, double loanInterestRate,
                double depositMaxLength, double senderFee, double senderAdditionalFee, int yearOfLicense) {
        super(name, address);
        this.currencies = currencies;
        this.converterLimit = converterLimit;
        this.converterFee = converterFee;
        this.loanLimit = loanLimit;
        this.loanInterestRate = loanInterestRate;
        this.depositMaxLength = depositMaxLength;
        this.senderFee = senderFee;
        this.senderAdditionalFee = senderAdditionalFee;
        this.yearOfLicense = yearOfLicense;
    }

    @Override
    public double convert(double amount, String userCurrency, String requiredCurrency) {

        double result = 0;

        if (userCurrency.equalsIgnoreCase("UAH")) {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(requiredCurrency)) {
                    result = (amount - converterFee) / currency.getRate();
                    if (amount <= converterLimit) {
                        return result;
                    } else {
                        return 0;
                    }
                }
            }
        } else {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(userCurrency)) {
                    result = amount * currency.getRate() - converterFee;
                    if (result <= converterLimit) {
                        return result;
                    } else {
                        return 0;
                    }

                }
            }
        }
        return result;
    }

    public String getAvailableCurrencies() {
        StringBuilder availableCurrencies = new StringBuilder();
        for (Currency currency : currencies) {
            availableCurrencies.append(currency.getName()).append(" ");
        }
        return availableCurrencies.toString();
    }

    @Override
    public String toString() {
        return String.format("%s, year of license: %s," +
                        "\n\tavailable currencies for convert: %s, converter fee: %s, converter limit: %s," +
                        "\n\tloan limit: %s, loan interest rate: %s," +
                        "\n\tdeposit max length: %s," +
                        "\n\tsender fee: %s, sender additional fee: %s",
                super.toString(), yearOfLicense, getAvailableCurrencies(), converterFee, converterLimit,
                loanLimit, loanInterestRate, depositMaxLength,
                senderFee, senderAdditionalFee);
    }

    @Override
    public boolean isLoanAvailable(double amount) {
        return amount <= loanLimit;
    }

    @Override
    public double getLoanInterestRate() {
        return loanInterestRate;
    }

    @Override
    public boolean isDepositAvailable(int lengthInMonths) {
        return lengthInMonths <= depositMaxLength;
    }

    @Override
    public double send(double money) {
        return (money * (1 - senderFee)) - senderAdditionalFee;
    }
}
