package eu.tsoml.model;

import eu.tsoml.util.Deposit;

public class InvestmentFund extends FinancialInstitution implements Deposit {

    private double yearOfFoundation;
    private int depositMinLength;

    public InvestmentFund(String name, String address, double yearOfFoundation, int depositMinLength) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
        this.depositMinLength = depositMinLength;
    }

    @Override
    public boolean isDepositAvailable(int lengthInMonths) {
        return lengthInMonths >= depositMinLength;
    }

    @Override
    public String toString() {
        return String.format("%s, year of foundation: %s, deposit min length: %s", super.toString(), yearOfFoundation, depositMinLength);
    }
}
