package eu.tsoml.model;

public abstract class FinancialInstitution {

    private String name;
    private String address;

    public FinancialInstitution(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name, address);
    }

}
