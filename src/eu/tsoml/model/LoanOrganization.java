package eu.tsoml.model;

import eu.tsoml.constant.LoanOrganizationType;
import eu.tsoml.util.Loan;

public class LoanOrganization extends FinancialInstitution implements Loan {

    private double loanLimit;
    private double loanInterestRate;
    private LoanOrganizationType type;

    public LoanOrganization(String name, String address, double loanLimit, double loanInterestRate, LoanOrganizationType type) {
        super(name, address);
        this.loanLimit = loanLimit;
        this.loanInterestRate = loanInterestRate;
        this.type = type;
    }

    @Override
    public boolean isLoanAvailable(double amount) {
        return amount <= loanLimit;
    }

    @Override
    public double getLoanInterestRate() {
        return loanInterestRate;
    }


    @Override
    public String toString() {
        return String.format("%s; loan limit: %s; loan interest rate: %s", super.toString(), loanLimit, loanInterestRate);
    }
}
