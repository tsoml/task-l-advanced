package eu.tsoml;


import eu.tsoml.data.DataFactory;
import eu.tsoml.model.*;
import eu.tsoml.sorter.LoanComparator;
import eu.tsoml.util.Converter;
import eu.tsoml.util.Deposit;
import eu.tsoml.util.Loan;
import eu.tsoml.util.Sender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class FinancialApp {

    static ArrayList<FinancialInstitution> arrayList = DataFactory.generate();

    public static void main(String[] args) {
        //task1();
        task2(50_000);
        //task3(12);
        //task4(1000);

    }

    public static void task1() {
        System.out.println("Task 1");
        Scanner scan = new Scanner(System.in);
        System.out.println("You have 20,000 UAH");
        System.out.print("Enter the currency to convert (USD,EUR or RUB): ");
        String requiredCurrency = scan.nextLine();

        double userMoney = 20_000;

        ArrayList<Converter> converterArrayList = new ArrayList<>();

        for (FinancialInstitution financialInstitution : arrayList) {
            if (financialInstitution instanceof Converter) {
                converterArrayList.add((Converter) financialInstitution);
            }
        }

        int bestResultId = 0;
        for (int i = 1; i < converterArrayList.size(); i++) {
            if (converterArrayList.get(i).convert(userMoney, "UAH", requiredCurrency) > converterArrayList.get(bestResultId).convert(userMoney, "UAH", requiredCurrency)) {
                Collections.swap(converterArrayList, i, bestResultId);
                bestResultId = i;
            }
        }

        for (Converter converter : converterArrayList) {
            double result = converter.convert(userMoney, "UAH", requiredCurrency);
            if (result != 0) {
                System.out.println(String.format("%.2f", result) + " • " + converter);
            }

        }
    }

    public static void task2(double amount) {
        System.out.println("Task 2");
        System.out.println("Minimum interest rate for a loan of " + amount + " UAH");

        ArrayList<Loan> loanArrayList = new ArrayList<>();

        for (FinancialInstitution financialInstitution : arrayList) {
            if (financialInstitution instanceof Loan) {
                loanArrayList.add((Loan) financialInstitution);
            }
        }

        for (int i = 0; i < loanArrayList.size(); i++) {
            if (loanArrayList.get(i).isLoanAvailable(amount)) {
                loanArrayList.sort(new LoanComparator());
                System.out.println(loanArrayList.get(i).getLoanInterestRate() + "% • " + loanArrayList.get(i));
            }
        }
    }


    public static void task3(int lengthInMonths) {
        System.out.println("Task 3");
        System.out.println("Best place to deposit for 12 months");

        for (FinancialInstitution financialInstitution : arrayList) {
            if (financialInstitution instanceof Deposit) {
                if (((Deposit) financialInstitution).isDepositAvailable(lengthInMonths)) {
                    System.out.println("• " + financialInstitution);
                }
            }
        }
    }

    public static void task4(double amount) {
        System.out.println("Task 4");
        System.out.println("The best organization for sending 1000 UAH");

        ArrayList<Sender> senderArrayList = new ArrayList<>();

        for (FinancialInstitution financialInstitution : arrayList) {
            if (financialInstitution instanceof Sender) {
                senderArrayList.add((Sender) financialInstitution);
            }
        }

        int bestResultId = 0;
        for (int i = 1; i < senderArrayList.size(); i++) {
            if (senderArrayList.get(i).send(amount) > senderArrayList.get(bestResultId).send(amount)) {
                Collections.swap(senderArrayList, i, bestResultId);
                bestResultId = i;
            }
        }

        for (Sender sender : senderArrayList) {
            System.out.println(sender.send(amount) + " • " + sender);
        }
    }

}
