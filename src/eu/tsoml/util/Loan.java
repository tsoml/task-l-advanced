package eu.tsoml.util;

public interface Loan {
    boolean isLoanAvailable(double amount);
    double getLoanInterestRate();
}
