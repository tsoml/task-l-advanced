package eu.tsoml.util;

public interface Deposit {
    boolean isDepositAvailable(int lengthInMonths);
}
