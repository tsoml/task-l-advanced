package eu.tsoml.util;

public interface Converter {
    double convert(double amount, String userCurrency, String requiredCurrency);
}
