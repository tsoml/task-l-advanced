package eu.tsoml.util;

public interface Sender {
    double send(double money);
}
