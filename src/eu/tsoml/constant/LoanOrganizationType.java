package eu.tsoml.constant;

public enum LoanOrganizationType {
    BANK, CREDIT_CAFE, CREDIT_UNION, PAWNSHOP;
}