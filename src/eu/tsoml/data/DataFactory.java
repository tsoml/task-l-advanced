package eu.tsoml.data;

import eu.tsoml.constant.LoanOrganizationType;
import eu.tsoml.model.*;

import java.util.ArrayList;

public class DataFactory {

    public static ArrayList<FinancialInstitution> generate() {

        ArrayList<FinancialInstitution> financialInstitutions = new ArrayList<>();

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("USD", 28);
            currencies[1] = new Currency("EUR", 29);
            currencies[2] = new Currency("UAH", 1);
            financialInstitutions.add(new Exchanger("Exchange", "Kharkov 17 st.", currencies));

        }

        {
            Currency[] currencies = new Currency[4];
            currencies[0] = new Currency("USD", 28.3);
            currencies[1] = new Currency("EUR", 29.3);
            currencies[2] = new Currency("RUB", 0.37);
            currencies[3] = new Currency("UAH", 1);
            financialInstitutions.add(new Bank("MonoBank", "Kharkov 33 st.", currencies, 150_000, 15,
                    200_000, 0.25, 12, 0.01, 25, 2010));
        }

        financialInstitutions.add(new LoanOrganization("Pawnshop", "Kharkov 9 st.", 50_000, 0.4, LoanOrganizationType.PAWNSHOP));
        financialInstitutions.add(new LoanOrganization("CreditCafe", "Kharkov 7 st.", 4_000, 2, LoanOrganizationType.CREDIT_CAFE));
        financialInstitutions.add(new LoanOrganization("CreditUnion", "Kharkov 5 st.", 100_000, 0.2, LoanOrganizationType.CREDIT_UNION));

        financialInstitutions.add(new InvestmentFund("Invest Fund", "Kharkov 1 st.", 2010, 12));
        financialInstitutions.add(new PostOffice("Nova Post", "Kharkov 1 st.", 0.02));
        financialInstitutions.add(new OnlineTranslationService("TT Online", "www.ttonline.com", 0.03));
        return financialInstitutions;
    }
}
