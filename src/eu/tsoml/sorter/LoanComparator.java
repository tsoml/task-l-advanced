package eu.tsoml.sorter;

import eu.tsoml.util.Loan;

import java.util.Comparator;

public class LoanComparator implements Comparator<Loan> {
    @Override
    public int compare(Loan l1, Loan l2) {
        double rate1 = l1.getLoanInterestRate();
        double rate2 = l2.getLoanInterestRate();
        return Double.compare(rate1, rate2);
    }
}
